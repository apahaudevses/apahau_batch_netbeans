package model;

import java.sql.*;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import module.Convert;

public class MysqlDb {
	/**
	 * DB接続情報
	 * 
	 * 初期はlocalhost
	 */
	  private String url = "jdbc:mysql://localhost/apahau_chintai?characterEncoding=utf8";
	  private String user = "root";
	  private String pass = "";
	  
	  private String host_name = "localhost";
	  
	  /**
	   * DB変数
	   */
	  private PreparedStatement ps = null;
	  private Connection con = null;
	  
	  //ログ変数
	  protected static Logger logger = Logger.getLogger( MysqlDb.class );
	  
	  //コンストラクト
	  public MysqlDb(String server){
		  if("local".equals(server)) {
			  url = "jdbc:mysql://localhost/apahau_chintai?characterEncoding=utf8";
			  user = "root";
			  pass = "";
		  }else if("dev".equals(server)) {
			  url = "jdbc:mysql://localhost/apahau_chintai?characterEncoding=utf8";
			  user = "root";
			  pass = "mode183sen";
		  }else if("batch".equals(server)) {
			  url = "jdbc:mysql://apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com/apahau_chintai?characterEncoding=utf8";
			  host_name = "apahaudb.cggn16sfiorv.ap-northeast-1.rds.amazonaws.com";
			  user = "apahau";
			  pass = "FByPy7Nn";
		  }
	  }
	  
	  public static void main(String[] args) throws SQLException {
		  try {
				//driverロード
				Class.forName("com.mysql.jdbc.Driver");
				
			} catch (ClassNotFoundException e){
				e.printStackTrace();
				
				//設定ファイルを読み込む
			  	PropertyConfigurator.configure("log4j.properties");
				logger.error("ドライバーロードエラー" + e.getMessage());
			}
	  }
	  
	  /**
	   * csvの更新日付とDBの更新日付を比較
	   * 
	   * @param idtext Housing_id(csv)
	   * @parama compare_date 比較日付(csv)
	   * @return boolean trueの時はcsvの方が新しい
	   * 
	   * TODO ★要確認(これでいいか)★
	   * DB:Converter_Update_Date
	   * csv: 部屋ﾃﾞｰﾀ最終更新日時
	   */
	  public boolean comparedate(String idtext,String compare_date) {
		  url = "jdbc:mysql://" + host_name + "/apahau_chintai?characterEncoding=utf8";
		  
		  try {
				con = DriverManager.getConnection(url,user,pass);
				String sql = "select Converter_Update_Date from T_Housing where Property_Management_Id = " + idtext;

				// ステートメントオブジェクトを生成
				ps = con.prepareStatement(sql);
				
				// クエリーを実行して結果セットを取得
				ResultSet rs = ps.executeQuery();
				
				String update_date = "";
				  
				// 検索された行数分ループ
				while(rs.next()) {
					//housing_idを取得
					update_date = rs.getString("Converter_Update_Date");
					
					//DB日付データ整形
		            String[] tmpArray;
		            tmpArray = update_date.split(" ");
		            String db_update = tmpArray[0].replaceAll("-", "");
					
					if(Integer.parseInt(compare_date) >= Integer.parseInt(db_update)) {
						//DBの方が日付が古いとき
						//System.out.println("true");
						return true;
					}
					else {
						return false;
					}
				}
				/* データベース切断 */
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
				out_logger_message("日付比較",e.getMessage());
				return false;
			}
		  finally{
				try{
					if (con != null){
						con.close();
				    }
				}catch (SQLException e){
				
				}
			}
		  	return false;
	  }
	  
	  /**
	   * User_id取得
	   * 
	   * @author masuda
	   * @param text Connected_1
	   */
	  public int get_userid(String text) {
		  String sql = "";
		  //MYSQL接続(文字コードをUTF-8指定で)
		  try {
			url = "jdbc:mysql://" + host_name + "/apahau_chintai?characterEncoding=utf8";
			con = DriverManager.getConnection(url,user,pass);
			
			sql = "select ID from T_ShopUser where Connected_1 = " + text;
			  
			  
			  // ステートメントオブジェクトを生成
	          ps = con.prepareStatement(sql);

	          // クエリーを実行して結果セットを取得
	          ResultSet rs = ps.executeQuery();

	          int user_id = -1;
	          
	          // 検索された行数分ループ
	          while(rs.next()) {
	        	  //housing_idを取得
	              user_id = rs.getInt("ID");

	              // データの表示
	              //System.out.println("user_id;"+" " + user_id);
	          }
			  /* データベース切断 */
			  con.close();

			  return user_id;
		} catch (SQLException e) {
			e.printStackTrace();
			
			out_logger_message(sql,e.getMessage()); 
			return -1;
		}finally{
			try{
				if (con != null){
					con.close();
			    }
			}catch (SQLException e){
			
			}
		}
	  }
	  
	  
	  /**
	   * Housing_id取得
	   * 
	   * @author masuda
	   * @param text Property_Management_Id
	   * @return housing_id
	   */
	  public int get_housingid(String text) {
		  String sql = "";
		  try {
			url = "jdbc:mysql://" + host_name + "/apahau_chintai?characterEncoding=utf8";
			con = DriverManager.getConnection(url,user,pass);
			
			sql = "select Housing_Id from T_Housing where Property_Management_Id = " + text;
			  
			  // ステートメントオブジェクトを生成
	          ps = con.prepareStatement(sql);

	          // クエリーを実行して結果セットを取得
	          ResultSet rs = ps.executeQuery();

	          int housing_id = -1;
	          
	          // 検索された行数分ループ
	          while(rs.next()) {
	        	  //housing_idを取得
	        	  housing_id = rs.getInt("Housing_Id");

	              // データの表示
	              //System.out.println("Housing_Id;"+" " + housing_id);
	          }
			  return housing_id;
		} catch (SQLException e) {
			e.printStackTrace();
			
			out_logger_message(sql,e.getMessage()); 
			return -1;
		}finally{
			try{
				if (con != null){
					con.close();
			    }
			}catch (SQLException e){
			
			}
		}
	  }
	  
	  
	  
	  
	  /**
	 　　　* INSERT用関数
	 　　　* 
	 　　　* @param inputmap インサート用hashmap
	 　　　* @return housing_id
	 　　　* @author masuda
	 　　　* 
	　　　 */
	    public int insert_db(String database,String tablename,HashMap<String,String> inputmap,String type) {
	    	String sql = "";
			try {
				//Hashが空の時でも入れるか?
				//if(!inputmap.isEmpty()){
					/*
					 * 変数
					 */
					StringBuilder field_text = new StringBuilder();
					StringBuilder value_text = new StringBuilder();
					String field_sql;
					String value_sql;
					
					switch (type) {
					case "db":
						//TODO 空の時はどうするか要確認
						url = "jdbc:mysql://" + host_name + "/" + database + "?characterEncoding=utf8";
						//MYSQL接続(文字コードをUTF-8指定で)
						con = DriverManager.getConnection(url,user,pass);
						/*
						 * SQL文を作成
						 */
						value_text.append("'");
						ResultSet rs                = null;
						int autoIncKey              = -1;
						
						for (String key:inputmap.keySet()) {
							//field名設定
							field_text.append(key);
							field_text.append(",");
							
							//value設定
							value_text.append(inputmap.get(key));
							value_text.append("','");
				        }
						
						//sql文連結
						//実行するSQL文とパラメータを指定する
						field_sql = field_text.toString().replaceAll(",$","");
						value_sql = value_text.toString().replaceAll(",'$","");
						sql = "INSERT INTO " + tablename +"(" + field_sql + ") VALUES(" + value_sql + ")";
						//System.out.println(sql);
						
						// クエリーを実行して結果セットを取得
						ps = con.prepareStatement(sql,java.sql.Statement.RETURN_GENERATED_KEYS);
				        ps.executeUpdate();
					    
						if(tablename.equals("T_Housing")) {
							//auto-incrementの値取得
							rs = ps.getGeneratedKeys();
							if (rs.next()) {
								autoIncKey = rs.getInt(1);
							}
							return autoIncKey;
						}
						break;
					case "text":
						value_text.append("'");
						
						for (String key:inputmap.keySet()) {
							//field名設定
							field_text.append(key);
							field_text.append(",");
							
							//value設定
							value_text.append(inputmap.get(key));
							value_text.append("','");
				        }
						
						//実行するSQL文とパラメータを指定する
						field_sql = field_text.toString().replaceAll(",$","");
						value_sql = value_text.toString().replaceAll(",'$","");
					
		
						sql = "INSERT INTO " + tablename +"(" + field_sql + ") VALUES(" + value_sql + ");";
						
						//INSERTのSQL文をテキストファイルで出力
						Convert convertobject = new Convert();
						convertobject.print_csv(tablename,field_text.toString(),sql,"INSERT");
						
						//System.out.println(sql);
						
						return -100;
//						break;
					default:
						break;
					}
				//}
				return -1;
			} catch (SQLException e) {
				e.printStackTrace();
				out_logger_message(sql,e.getMessage());
				return -1;
			}finally{
				try{
					if (con != null){
						con.close();
				    }
				}catch (SQLException e){
				
				}
			}
	    }
	    
	    /**
	     * UPDATE用関数
	     * 
	     * @param dbname データベース名
	     * @param inputmap update用hashmap
	     * @param housing_id ハウジングID
	     * @return void
	     * @author masuda
	     * 
	     * 
	     */
	    public void update_db(String database,String[] sql_array,int housing_id,String type) {
	    	String tmpText = "";
	    	int exist_flag = 1;
	    	
	    	try {
    			StringBuffer sql;
    			switch (type) {
				case "db":
	    			//hashがからでないときは実行
					url = "jdbc:mysql://" + host_name + "/" + database + "?characterEncoding=utf8";
					
					PreparedStatement ps = null;
					//MYSQL接続(文字コードをUTF-8指定で)
					con = DriverManager.getConnection(url,user,pass);
					
					
					for(String value : sql_array) {
						if(!value.equals("")) {
							//配列のSQL文を実行
							//System.out.println(value);
					        
							tmpText = value;
					        // ステートメントオブジェクトを生成
					        ps = con.prepareStatement(value);
			
					        // クエリーを実行して結果セットを取得
					        exist_flag = ps.executeUpdate();
					        
					        if(exist_flag == 0) {
					        	//ログ肥大化対策のためコメントアウト
					        	//out_logger_message(tmpText,"物件が見つからなかった");
					        }
						}
    				}
			        
			        break;
				case "text":
		        
					break;
				default:
					break;
    			}
			} catch (SQLException e) {
				e.printStackTrace();
				out_logger_message(tmpText,e.getMessage()); 
			}finally{
				try{
					if (con != null){
						con.close();
				    }
				}catch (SQLException e){
				
				}
			}
	    }
	    
	    
	    /**
	     * DELETE用関数
	     * @param id Property_Management_Id/Housing_Id
	     * @return void
	     * @author masuda
	     * 
	     * 
	     */
	    public void delete_db(String id,String type) throws SQLException {
	    	String tmpText = "";
	    	PreparedStatement ps = null;
	    	String sql;
	    	sql = "";
	    	int housing_id = 0;
	    	int exist_flag = 1;
	    	
	    	try {
	    			switch (type) {
					case "db":
						housing_id = get_housingid(id);
						
						url = "jdbc:mysql://" + host_name + "/apahau_chintai?characterEncoding=utf8";
						ps = null;
						//MYSQL接続(文字コードをUTF-8指定で)
						con = DriverManager.getConnection(url,user,pass);
						
						/**
						 * 論理削除
						 */
						sql = "UPDATE T_Housing SET Delete_FLG = '1' where Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
				        exist_flag = ps.executeUpdate();
				        
				        //System.out.println(sql);

				        if(exist_flag == 0) {
				        	//ログ肥大化対策のためコメントアウト
				        	//out_logger_message(sql,"物件が見つからなかった");
				        }

				        //いったん閉じる
				        con.close();
				        
						//hashがからでないときは実行
						url = "jdbc:mysql://" + host_name + "/apahau_search?characterEncoding=utf8";
						ps = null;
						
						//MYSQL接続(文字コードをUTF-8指定で)
						con = DriverManager.getConnection(url,user,pass);
						
						/**
						 * 論理削除
						 */
						sql = "UPDATE T_SearchHousing SET Delete_FLG = '1' WHERE Housing_Id = '" + String.valueOf(housing_id) + "'";
						ps = con.prepareStatement(sql.toString());
						exist_flag = ps.executeUpdate();

						if(exist_flag == 0) {
							//ログ肥大化対策のためコメントアウト
							//out_logger_message(sql,"物件が見つからなかった");
				        }
				        
				        break;
					case "db_physic":
						url = "jdbc:mysql://" + host_name + "/apahau_chintai?characterEncoding=utf8";
						ps = null;
						//MYSQL接続(文字コードをUTF-8指定で)
						con = DriverManager.getConnection(url,user,pass);
						
						/**
						 * 物理削除
						 */
						//T_Housing
						sql = "DELETE FROM T_Housing WHERE Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
				        exist_flag = ps.executeUpdate();
				        
				        //T_Equipment
						sql = "DELETE FROM T_Equipment WHERE Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
				        exist_flag = ps.executeUpdate();
				        
				        //T_ChatchCopy
						sql = "DELETE FROM T_ChatchCopy WHERE Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
				        exist_flag = ps.executeUpdate();
				        
				        //T_Image
						sql = "DELETE FROM T_Image WHERE Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
				        exist_flag = ps.executeUpdate();
				        
				        //T_NearbyAttractions
						sql = "DELETE FROM T_NearbyAttractions WHERE Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
				        exist_flag = ps.executeUpdate();
				        
				        //T_RoomInfo
						sql = "DELETE FROM T_RoomInfo WHERE Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
				        exist_flag = ps.executeUpdate();
				        
				        //T_Traffic
						sql = "DELETE FROM T_Traffic WHERE Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
				        exist_flag = ps.executeUpdate();
				        
				        //T_Article
						sql = "DELETE FROM T_Article WHERE Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
				        exist_flag = ps.executeUpdate();
				        
				        //System.out.println(sql);

				        if(exist_flag == 0) {
				        	//ログ肥大化対策のためコメントアウト
				        	//out_logger_message(sql,"物件が見つからなかった");
				        }

				        //いったん閉じる
				        con.close();
				        
						//hashがからでないときは実行
						url = "jdbc:mysql://" + host_name + "/apahau_search?characterEncoding=utf8";
						ps = null;
						
						//MYSQL接続(文字コードをUTF-8指定で)
						con = DriverManager.getConnection(url,user,pass);
						
						/**
						 * 物理削除
						 */
						//T_SearchHousing
						sql = "DELETE FROM T_SearchHousing WHERE Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
						exist_flag = ps.executeUpdate();
						
						//T_SearchTraffic
						sql = "DELETE FROM T_SearchTraffic WHERE Housing_Id = '" + String.valueOf(id) + "'";
						ps = con.prepareStatement(sql.toString());
						exist_flag = ps.executeUpdate();

						if(exist_flag == 0) {
							//ログ肥大化対策のためコメントアウト
							//out_logger_message(sql,"物件が見つからなかった");
				        }
				        
				        break;
					case "text":
				        //デバッグ用にINSERTする置換データ出力(CSV形式)
						sql = "UPDATE T_Housing SET Delete_FLG = '1' where Property_Management_Id = '" + String.valueOf(id) + "';\n";
						sql = sql + "UPDATE T_SearchHousing SET Delete_FLG = '1' WHERE Housing_Id = '" + String.valueOf(housing_id) + "';";
						
						//System.out.println(sql);
						
						
						//SQL文　ファイル出力
						Convert convertobject = new Convert();
						convertobject.print_csv("delete","",sql,"DELETE");
						break;
					default:
						break;
	    			}
			} catch (SQLException e) {
				e.printStackTrace();
				out_logger_message(tmpText,e.getMessage());
			}finally{
				try{
					if (con != null){
						con.close();
				    }
				}catch (SQLException e){
				
				}
			}
	    }
	    
	    /**
	     * ロガー出力関数(error)
	     * 
	     * @author masuda
	     * @param sql sql文
	     * @param message エラーメッセージ
	     */
	    public void out_logger_message(String sql,String message) {
	    	//設定ファイルを読み込む
		  	PropertyConfigurator.configure("log4j.properties");
		  	
	    	String log_message = "(1)" + sql + "\n" + "(2)" + message;
			logger.error(log_message);
	    }
}

