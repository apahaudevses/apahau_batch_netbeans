package controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

import model.MysqlDb;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 実際の処理プログラム(DELETE文)
 * @author masuda
 * 
 * ●コマンド
 * データベースインサート
 * java -jar delete local db
 * テキスト出力
 * java -jar delete local text
 * 
 */
public class Delete {
	//ロガー
	protected static Logger logger = Logger.getLogger( Delete.class );
	
	public static void main(String[] args) {
		//jar実行パス
		final String EXECPATH = System.getProperty("user.dir");
		//final String DELIMITER = "\\";
		final String DELIMITER = "/";
		
		InputStreamReader isr = null;
		BufferedReader br = null;

		//SQL実行　物件数　カウンター
		int bukken_counter;
		bukken_counter = 0;
		
		try {
//			isr = new InputStreamReader(new FileInputStream(EXECPATH + DELIMITER + "input_csv" + DELIMITER + "delete.csv"),"Shift_JIS");
			isr = new InputStreamReader(new FileInputStream(EXECPATH + DELIMITER + "input_csv" + DELIMITER + "delete.csv"),"UTF-8");
			br = new BufferedReader( isr );
			
			//読み込んだ行を保存
			String line;
			
			/*
			 * それぞれのDB用インスタンス生成
			 */
			MysqlDb mysqlobject = new MysqlDb(args[0]);
			
			//ファイルの件数分ループして配列に格納します。
			while ((line = br.readLine()) != null && !line.equals("")) {
				//System.out.println("●１ループ開始●");
				mysqlobject.delete_db(line,args[1]);
				//System.out.println("●１ループ終了●");
				
				bukken_counter++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
			//設定ファイルを読み込む
			PropertyConfigurator.configure("log4j.properties");
			logger.error(e.getMessage());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
			//設定ファイルを読み込む
			PropertyConfigurator.configure("log4j.properties");
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			
			//設定ファイルを読み込む
			PropertyConfigurator.configure("log4j.properties");
			logger.error(e.getMessage());
		}finally {
			//SQL実行物件数 表示
			System.out.println("DELETE実行 物件数");
			System.out.println(bukken_counter);
			
			//設定ファイルを読み込む
			PropertyConfigurator.configure("log4j_debug.properties");
			logger.debug("DELETE実行 物件数" + bukken_counter );
			
			if (br != null) try { br.close(); } catch (IOException e) {}
			if (isr != null) try { isr.close(); } catch (IOException e) {}
		}
	}
}