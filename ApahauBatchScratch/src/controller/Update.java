package controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import model.MysqlDb;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 実際の処理プログラム(UPDATE文)
 * @author masuda
 * 
 * ●コマンド
 * データベースアップデート
 * java -jar update local db
 * テキスト出力
 * java -jar update local text
 *
 */
public class Update {
	//ロガー
	protected static Logger logger = Logger.getLogger( Update.class ); 

	public static void main(String[] args) {
		//jar実行パス
		final String EXECPATH = System.getProperty("user.dir");
		//final String DELIMITER = "\\";
		final String DELIMITER = "/";

		InputStreamReader isr = null;
		BufferedReader br = null;

		//SQL実行　物件数　カウンター
		int bukken_counter;
		bukken_counter = 0;

		try {
			MysqlDb mysqlobject = new MysqlDb(args[0]); //mysql

			//isr = new InputStreamReader(new FileInputStream(EXECPATH + DELIMITER + "input_csv" + DELIMITER + "update.csv"),"Shift_JIS");
			isr = new InputStreamReader(new FileInputStream(EXECPATH + DELIMITER + "input_csv" + DELIMITER + "update.csv"),"UTF-8");
			br = new BufferedReader( isr );

			//読み込んだ行を保存
			//String line = br.readLine();
			String line;
			
			//アパハウcsvを入れておく配列
			String apamanarray[];

			int housing_id;
			housing_id = -1;

			/*
			 * それぞれのDB用インスタンス生成
			 */
			T_SearchHousing t_searchhousing_obj = new T_SearchHousing(args[0],args[1]);
			T_ChatchCopy t_chatchcopy = new T_ChatchCopy(args[0],args[1]);
			T_Article t_article = new T_Article(args[0],args[1]);
			T_Equipment t_equipment = new T_Equipment(args[0],args[1]);
			T_Image t_image = new T_Image(args[0],args[1]);
			T_RoomInfo t_roominfo = new T_RoomInfo(args[0],args[1]);
			T_NearbyAttractions t_nearbyattractions = new T_NearbyAttractions(args[0],args[1]);
			T_Traffic t_traffic = new T_Traffic(args[0],args[1]);
			T_SearchTraffic t_searchtraffic = new T_SearchTraffic(args[0],args[1]);
			T_Housing t_housing = new T_Housing(args[0],args[1]);

			//ファイルの件数分ループして配列に格納します。
			while ((line = br.readLine()) != null ) {
				if(!line.equals("")) {
					//タブで分割にする
					line = line.replaceAll("\",\"","\t");
					line = line.replaceAll("^\"","");
					line = line.replaceAll("\"$","");
					
					//分割配列作成(タブ)
					apamanarray = line.split("\t");
					
					//apahau_chintai アップデートSQL 配列
					String[] apahau_chintai_sql = new String[8];
					//apahau_chintai アップデートSQL 配列
					String[] apahau_search_sql = new String[2];
					
					//送信元更新日時がcsvより古いとき
					//TODO ★csv生成時に変更したから　比較いらないかも★
					//if(mysqlobject.comparedate(apamanarray[0],apamanarray[191])) {
						/*
						 * 実際の変換処理
						 */
						//System.out.println("●１ループ開始●");
						
						//Housing_id取得
						housing_id = mysqlobject.get_housingid(apamanarray[0]);

						//T_HOUSING変換(ハウジングIDを取得するため1回SQL文実行)
						apahau_chintai_sql[0] = t_housing.update(housing_id,apamanarray);
						//T_Traffic変換
						apahau_chintai_sql[1] = t_traffic.update(housing_id,apamanarray);
						//T_NearbyAttractions
						apahau_chintai_sql[2] = t_nearbyattractions.update(housing_id,apamanarray);
						//T_Image
						apahau_chintai_sql[3] = t_image.update(housing_id,apamanarray);
						//T_RoomInfo
						apahau_chintai_sql[4] = t_roominfo.update(housing_id,apamanarray);
						//T_Equipment
						apahau_chintai_sql[5] = t_equipment.update(housing_id,apamanarray);
						//T_ChatchCopy
						apahau_chintai_sql[6] = t_chatchcopy.update(housing_id,apamanarray);
						//T_Article
						apahau_chintai_sql[7] = t_article.update(housing_id,apamanarray);

						//●apahau_chintaiデータベースUPDATE
						mysqlobject.update_db("apahau_chintai",apahau_chintai_sql, housing_id, args[1]);
						//T_SearchHousing
						apahau_search_sql[0] = t_searchhousing_obj.update(housing_id,apamanarray);
						//T_SearchTraffic
						apahau_search_sql[1] = t_searchtraffic.update(housing_id,apamanarray);

						//●apahau_searchデータベースUPDATE
						mysqlobject.update_db("apahau_search",apahau_search_sql, housing_id, args[1]);

						//System.out.println("●１ループ終了●");

						bukken_counter++;
						//System.out.println(bukken_counter);
					//}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();

			//設定ファイルを読み込む
		  	PropertyConfigurator.configure("log4j.properties");
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();

			//設定ファイルを読み込む
		  	PropertyConfigurator.configure("log4j.properties");
			logger.error(e.getMessage());
		}finally {
			//SQL実行物件数 表示
			System.out.println("UPDATE実行 物件数");
			System.out.println(bukken_counter);

			//設定ファイルを読み込む
		  	PropertyConfigurator.configure("log4j_debug.properties");
		  	logger.debug("UPDATE実行 物件数" + bukken_counter );
			
			if (br != null) try { br.close(); } catch (IOException e) {}
			if (isr != null) try { isr.close(); } catch (IOException e) {}
		}
	}
}